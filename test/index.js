var assert = require('assert');
var ConnectedEmitters = require('origami-connected-emitters');
var SocketInitializer = require('..');

describe('SocketInitializer', function () {
  it('requires initializer function', function () {
    assert.throws(
      function () {
        new SocketInitializer();
      },
      /initializer is required/
    );
  });

  it('accepts initializer', function () {
    new SocketInitializer(function () {});
  });

  describe('.initialize', function () {
    it('invokies initializer', function (done) {
      var ce1 = new ConnectedEmitters();
      
      var handler = new SocketInitializer(
        function (socket) {
          try {
            assert(socket);

            done();
          } catch (e) {
            done(e);
          }
        });

        handler.handle(ce1.createEmitter());
    });

    it('waits for initializer resolution', function (done) {
      var ce1 = new ConnectedEmitters();

      var handled = false;

      var handler = new SocketInitializer(
        function (socket) {
          handled = true;

          return Promise.resolve();
        });

      handler
      .handle(ce1.createEmitter())
      .then(function () {
        try {
          assert(handled);

          done();
        } catch (e) {
          done(e);
        }
      });
    });

    it('passes additional arguments to the initializer', function (done) {
      var ce1 = new ConnectedEmitters();
      var mockObject = {};
      
      var handler = new SocketInitializer(
        function (socket, arg1, arg2) {
          try {
            assert.equal('some-string', arg1);
            assert.equal(mockObject, arg2);
            
            done();
          } catch (e) {
            done(e);
          }
        });

      handler
      .handle(ce1.createEmitter(), 'some-string', mockObject);
    });

    it('queues incoming messages until ready is called', function (done) {
      var ce1 = new ConnectedEmitters();
      var originalSocket = ce1.createEmitter();
      var ready = false;

      var handler = new SocketInitializer(
        function (socket) {
          socket.on('hello1', function () {
            try {
              assert(ready);

              done();
            } catch (e) {
              done(e);
            }
          });

          originalSocket.emit('hello1');

          return Promise.resolve();
        });


      handler
      .handle(ce1.createEmitter())
      .then(function (markReady) {
        ready = true;
        markReady();
      });
    });

    it('queues outgoing messages until it is ready is called', function (done) {
      var ce1 = new ConnectedEmitters();
      var originalSocket = ce1.createEmitter();
      var ready = false;

      var handler = new SocketInitializer(
        function (socket) {
          originalSocket.on('hello1', function () {
            try {
              assert(ready);

              done();
            } catch (e) {
              done(e);
            }
          });

          socket.emit('hello1');

          return Promise.resolve();
        });


      handler
      .handle(ce1.createEmitter())
      .then(function (markReady) {
        ready = true;
        markReady(); 
      });
    });

    it('initialization still queues incoming messages', function (done) {
      var ce1 = new ConnectedEmitters();
      var emiited = false;
      var originalSocket = ce1.createEmitter();

      var handler = new SocketInitializer(
        function (socket) {
          socket.on('hello1', function () {
            emiited = true;
          });

          return Promise.resolve();
        });


      handler
      .handle(ce1.createEmitter())
      .then(function () {
        originalSocket.emit('hello1');
        
        try {
          assert(!emiited);
          
          done();
        } catch (e) {
          done(e);
        }
      });
    });

    it('initialization still queues messages', function (done) {
      var emitted = false;
      var ce1 = new ConnectedEmitters();
      var originalSocket = ce1.createEmitter();

      originalSocket.on('hello1', function () {
        emitted = true;
      });

      var secondSocket;

      var handler = new SocketInitializer(
        function (socket) {
          secondSocket = socket;
      
          return Promise.resolve();
        });


      handler
      .handle(ce1.createEmitter())
      .then(function (markReady) {
        secondSocket.emit('hello1');
        
        try {
          assert(!emitted);
        
          done();
        } catch (e) {
          done(e);
        }
      });
    });
  });
});
