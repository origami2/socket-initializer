var async = require('async');
var ConnectedEmitters = require('origami-connected-emitters');

function SocketInitializer(initializer) {
  if (!initializer) throw new Error('initializer is required');

  this.initializer = initializer;
}

SocketInitializer.prototype.handle = function (socket) {
  var self = this;

  var ce = new ConnectedEmitters();
  var facadeSocket = ce.createEmitter();
  var mySocket = ce.createEmitter();
  
  var handleArgs = [facadeSocket];

  for (var i = 1; i < arguments.length; i++) {
    handleArgs.push(arguments[i]);
  }

  return new Promise(function (resolve, reject) {
    var outgoingQueue = async.queue(function (task, callback) {
      socket.emit.apply(socket, task.args);
      callback();
    });

    outgoingQueue.pause();

    var outgoingQueueListener = function () {
      var args = [];

      for (var i = 0; i < arguments.length; i++) {
        args.push(arguments[i]);
      }

      outgoingQueue.push({args: args});
    };

    mySocket.onAny(outgoingQueueListener);

    var incomingQueue = async.queue(function (task, callback) {
      mySocket.emit.apply(mySocket, task.args);
      callback();
    });

    incomingQueue.pause();

    var incomingQueueListener = function () {
      var args = [];

      for (var i = 0; i < arguments.length; i++) {
        args.push(arguments[i]);
      }

      incomingQueue.push({args: args});
    };

    socket.onAny(incomingQueueListener);

    self
    .initializer
    .apply(self.initializer, handleArgs)
    .then(function () {
      resolve(function () {
        // ready function
  
        socket.offAny(incomingQueueListener);
        mySocket.offAny(outgoingQueueListener);
  
        incomingQueue.resume();
        outgoingQueue.resume();
  
        Promise.all(
          [
            new Promise (function (resolve, reject) {
              if(incomingQueue.length() === 0) return resolve();
  
              incomingQueue.drain = function () {
                incomingQueue.drain = function () {};
  
                resolve();
              };
            }),
            new Promise (function (resolve, reject) {
              if(outgoingQueue.length() === 0) return resolve();
  
              outgoingQueue.drain = function () {
                outgoingQueue.drain = function () {};
  
                resolve();
              };
            })
          ]
        )
        .then(function () {
          var forwardIncoming = function () {
            var args = [];
  
            for (var i = 0; i < arguments.length; i++) {
              args.push(arguments[i]);
            }
  
            mySocket.emit.apply(mySocket, args);
            };
          var forwardOutgoing = function () {
            var args = [];
  
            for (var i = 0; i < arguments.length; i++) {
              args.push(arguments[i]);
            }
  
            socket.emit.apply(socket, args);
          };
  
          mySocket.onAny(forwardOutgoing);
          socket.onAny(forwardIncoming);
        });
      });
    })
    .catch(reject);
  });
};

module.exports = SocketInitializer;
